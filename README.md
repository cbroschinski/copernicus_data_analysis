





## 1. Datengrundlage

Alle folgenden Auswertungen basieren auf dem OpenAPC-Kerndatensatz (apc_de.csv) und der von Copernicus bereitgestellten Tabelle.

Die Datei 'Final-papers-German-institutions-2016_enriched.csv' entspricht der von Copernicus bereitgestellten Originaldatei, erweitert um 5 Spalten:

- normalised_affiliation: Normalisierte Institutionenansetzung, abgeleitet aus der Spalte "Affiliation". Es wurden fast alle Einträge normalisiert, mit Ausnahme nicht zuzuordnender Adressen und kleiner privatwirtschaftlicher Einrichtungen, die für OpenAPC nicht von Belang sind. Die Datei 'affiliations.csv' enthält die mappings.  
- association: Zuordnung der Institution zu einer Organisation, falls anwendbar. Optionen sind HGF (Helmholtz), WGL (Leibniz), MPG (Max Planck), FHG (Fraunhofer) oder federal (Deutsche Bundesbehörde).
- institutional agreement: institutional agreement nach Angabe von Copernicus (grüne oder blaue Zeilen in Originaltabelle).
- in_openapc_according_to_copernicus: Vorhandensein des Artikels in OpenAPC nach Angabe von Copernicus (rote oder blaue Zeilen in Originaltabelle).
- in_openapc: Vorhandensein des Artikels in OpenAPC nach eigener Analyse.



## 2. Analyse

### 2.1 Nicht-teilnehmende Institutionen

Die erste Frage war, welche Institutionen noch nicht Teilnehmer bei OpenAPC sind. Das Ergebnis zeigt die folgende Tabelle:

(Anmerkungen: DLR = Deutsches Zentrum für Luft- und Raumfahrt, AWI = Alfred-Wegener-Institut, HZG = Helmholtz-Zentrum Geesthacht, PIK = Potsdam-Institut für Klimafolgenforschung)


|Institution                  |Organisation | Anzahl Artikel|
|:----------------------------|:------------|--------------:|
|GEOMAR                       |HGF          |             33|
|TROPOS                       |WGL          |             31|
|DLR                          |HGF          |             27|
|AWI                          |HGF          |             22|
|HZG                          |HGF          |             16|
|PIK                          |WGL          |             16|
|Frankfurt U                  |-            |             11|
|Deutscher Wetterdienst       |Federal      |             10|
|TU Braunschweig              |-            |             10|
|Bonn U                       |-            |              8|
|Kiel U                       |-            |              7|
|Koeln U                      |-            |              7|
|Hamburg U                    |-            |              6|
|Thuenen                      |-            |              6|
|EUMETSAT                     |-            |              5|
|German Primate Center        |WGL          |              5|
|Hohenheim U                  |-            |              4|
|IAP                          |WGL          |              4|
|IASS                         |-            |              4|
|Jena U                       |-            |              4|
|RWTH                         |-            |              4|
|AUDI                         |-            |              3|
|Augsburg U                   |-            |              3|
|BfG                          |Federal      |              3|
|LIAG                         |WGL          |              3|
|PTB                          |Federal      |              3|
|Trier U                      |-            |              3|
|BGR                          |Federal      |              2|
|Eichstätt U                  |-            |              2|
|HZM                          |HGF          |              2|
|IKTS                         |FHG          |              2|
|IOW                          |WGL          |              2|
|Marburg U                    |-            |              2|
|MFN                          |WGL          |              2|
|Paderborn U                  |-            |              2|
|Senckenberg DZMB             |WGL          |              2|
|Stuttgart U                  |-            |              2|
|TU Darmstadt                 |-            |              2|
|TU Freiberg                  |-            |              2|
|TUHH                         |-            |              2|
|Wuppertal U                  |-            |              2|
|AIP                          |WGL          |              1|
|Beuth HS                     |-            |              1|
|Bochum HS                    |-            |              1|
|Bremen Jacobs                |-            |              1|
|Deutsches Klimarechenzentrum |-            |              1|
|Duesseldorf U                |-            |              1|
|Geisenheim U                 |-            |              1|
|Greifswald U                 |-            |              1|
|Halle-Wittenberg U           |-            |              1|
|HU Berlin                    |-            |              1|
|IAMO                         |WGL          |              1|
|IFW                          |WGL          |              1|
|IGB                          |FHG          |              1|
|IPM                          |FHG          |              1|
|IRS                          |WGL          |              1|
|IWS                          |FHG          |              1|
|Koblenz U                    |-            |              1|
|Magdeburg U                  |-            |              1|
|OWL HS                       |-            |              1|
|Reutlingen U                 |-            |              1|
|Rostock U                    |-            |              1|
|Saarland U                   |-            |              1|
|ZALF                         |WGL          |              1|
|ZMT                          |WGL          |              1|

Insgesamt handelt es sich um 65 Institutionen mit einem Gesamtbestand von 310 unerschlossenen Artikeln.

### 2.2 Vollständigkeit bei teilnehmenden Institutionen

Die zweite Frage war die nach der Vollständigkeit der Meldung der Copernicus-Artikel durch teilnehmende Institutionen. Prinzipiell lässt sich diese Frage analysieren, allerdings muss beim Betrachten der Ergebnistabelle folgendes beachtet werden:

* Der Abgleich basiert auf der DOI, d.h. es wird überprüft, ob eine Copernicus-DOI in OpenAPC vorhanden ist.
* Sogenannte "Discussions"-Fälle, bei denen eine Institution die "discussions"-DOI eines Artikels an OpenAPC gemeldet hat, in der Copernicus-Tabelle allerdings die DOI zum eigentlichen Artikel steht, werden nicht korrekt erkannt (Größenordnung sind allerdings lediglich 4-5 Fälle).
* Die Affiliation der Artikel ist in einigen Fällen problematisch. Bei der Analyse wird lediglich überprüft, ob eine DOI überhaupt in OpenAPC vorhanden ist, die Institution wird dabei nicht gesondert überprüft. Es sei jedoch angemerkt, dass diese nicht in allen Fällen übereinstimmt, siehe Abschnitt 2.2.1.
* Max-Planck-Institute werden in einer gesonderten Tabelle betrachtet, da diese in OpenAPC durch die MDPL unspezifisch als "MPG" gemeldet werden, bei Copernicus ist die Aufschlüsselung genauer.


|Institution          |Organisation | in OpenAPC| nicht in OpenAPC| Artikel gesamt| OpenAPC-Abdeckung(%)|
|:--------------------|:------------|----------:|----------------:|--------------:|--------------------:|
|KIT                  |-            |         24|               20|             44|                 54.5|
|GFZ-Potsdam          |HGF          |         12|               12|             24|                 50.0|
|Bremen U             |-            |         21|                2|             23|                 91.3|
|FZJ - ZB             |HGF          |          9|               14|             23|                 39.1|
|Muenchen LMU         |-            |          2|               12|             14|                 14.3|
|TU Muenchen          |-            |          8|                5|             13|                 61.5|
|TU Dresden           |-            |          0|               13|             13|                  0.0|
|Freiburg U           |-            |          1|               10|             11|                  9.1|
|Hannover U           |-            |          5|                4|              9|                 55.6|
|Oldenburg U          |-            |          1|                8|              9|                 11.1|
|Leipzig U            |-            |          3|                5|              8|                 37.5|
|FU Berlin            |-            |          3|                4|              7|                 42.9|
|JGU Mainz            |-            |          3|                4|              7|                 42.9|
|Giessen U            |-            |          1|                5|              6|                 16.7|
|Heidelberg U         |-            |          3|                3|              6|                 50.0|
|Bayreuth U           |-            |          0|                6|              6|                  0.0|
|Tuebingen U          |-            |          2|                3|              5|                 40.0|
|UFZ                  |HGF          |          2|                3|              5|                 40.0|
|Bochum U             |-            |          0|                5|              5|                  0.0|
|TU Berlin            |-            |          0|                5|              5|                  0.0|
|Erlangen Nuernberg U |-            |          2|                2|              4|                 50.0|
|Dortmund TU          |-            |          0|                4|              4|                  0.0|
|Potsdam U            |-            |          0|                4|              4|                  0.0|
|Goettingen U         |-            |          1|                2|              3|                 33.3|
|HZDR                 |HGF          |          2|                1|              3|                 66.7|
|Konstanz U           |-            |          1|                0|              1|                100.0|
|Münster U            |-            |          1|                0|              1|                100.0|
|Duisburg-Essen U     |-            |          0|                1|              1|                  0.0|
|Kassel U             |-            |          0|                1|              1|                  0.0|
|TiHo Hannover        |-            |          0|                1|              1|                  0.0|
|TU Chemnitz          |-            |          0|                1|              1|                  0.0|

Sonderfall Max-Planck-Institute (Im OpenAPC-Datensatz zusammengefasst unter "MPG", hier einzeln aufgeführt)


|Institution |Organisation | in OpenAPC| nicht in OpenAPC| Artikel gesamt| OpenAPC-Abdeckung(%)|
|:-----------|:------------|----------:|----------------:|--------------:|--------------------:|
|MPIC        |MPG          |         36|                5|             41|                   88|
|MPI-M       |MPG          |         12|                4|             16|                   75|
|BGC-Jena    |MPG          |          7|                2|              9|                   78|
|MPS         |MPG          |          1|                0|              1|                  100|
|MPE         |MPG          |          0|                1|              1|                    0|

Zur Einzelanalyse hier die nicht an OpenAPC gemeldeten MPG-Artikel:


|Affiliation |DOI                       |Journal                            |
|:-----------|:-------------------------|:----------------------------------|
|BGC-Jena    |10.5194/esd-7-71-2016     |Earth System Dynamics              |
|BGC-Jena    |10.5194/bg-13-925-2016    |Biogeosciences                     |
|MPE         |10.5194/hgss-7-67-2016    |History of Geo- and Space Sciences |
|MPI-M       |10.5194/esurf-4-407-2016  |Earth Surface Dynamics             |
|MPI-M       |10.5194/cp-12-2145-2016   |Climate of the Past                |
|MPI-M       |10.5194/esd-7-1-2016      |Earth System Dynamics              |
|MPI-M       |10.5194/esd-7-535-2016    |Earth System Dynamics              |
|MPIC        |10.5194/acp-16-10175-2016 |Atmospheric Chemistry and Physics  |
|MPIC        |10.5194/gmd-9-3875-2016   |Geoscientific Model Development    |
|MPIC        |10.5194/acp-16-6381-2016  |Atmospheric Chemistry and Physics  |
|MPIC        |10.5194/amt-9-5183-2016   |Atmospheric Measurement Techniques |
|MPIC        |10.5194/acp-16-13035-2016 |Atmospheric Chemistry and Physics  |


#### 2.2.1 Abweichende Affiliation

Es gibt insgesamt 171 DOI-Übereinstimmungen zwischen den Copernicus-Daten und OpenAPC, allerdings stimmt nicht bei allen die Affiliation überein. Bei 15 Artikeln gibt es eine Abweichung:


|DOI                       |Organisation (Copernicus) |Affiliation (Copernicus) |Affiliation (OpenAPC)                       |
|:-------------------------|:-------------------------|:------------------------|:-------------------------------------------|
|10.5194/bg-13-1553-2016   |-                         |Leipzig U                |MPG                                         |
|10.5194/se-7-1521-2016    |-                         |Muenchen LMU             |TU Muenchen                                 |
|10.5194/amt-9-4181-2016   |-                         |Muenchen LMU             |OpenAIRE                                    |
|10.5194/hess-20-3441-2016 |-                         |Jena U                   |MPG                                         |
|10.5194/hess-20-2779-2016 |-                         |Freiburg U               |OpenAIRE                                    |
|10.5194/bg-13-3163-2016   |-                         |Heidelberg U             |MPG                                         |
|10.5194/acp-16-9591-2016  |-                         |Bremen U                 |MPG                                         |
|10.5194/os-12-899-2016    |WGL                       |IOW                      |Leibniz-Fonds                               |
|10.5194/acp-16-8593-2016  |WGL                       |TROPOS                   |MPG                                         |
|10.5194/acp-16-9831-2016  |WGL                       |TROPOS                   |Leibniz-Fonds                               |
|10.5194/amt-9-3707-2016   |WGL                       |IAP                      |Leibniz-Fonds                               |
|10.5194/cp-12-1995-2016   |-                         |Frankfurt U              |Bremen U                                    |
|10.5194/bg-13-3461-2016   |WGL                       |Senckenberg DZMB         |Leibniz-Fonds                               |
|10.5194/bg-13-1119-2016   |-                         |Thuenen                  |Swedish University of Agricultural Sciences |
|10.5194/bg-13-659-2016    |-                         |Konstanz U               |MPG                                         |

### 2.3 Kostenabschätzung für Nicht-Teilnehmer



Die dritte Fragestellung ist die nach einer Kostenabschätzung für Nicht-Teilnehmer, basierend auf den in OpenAPC verzeichneten Durchschnittskosten.
In einem ersten Schritt wurde hierzu die Copernicus-Datei mit den OpenAPC-Standardskripten aus Crossref angereichert, um OpenAPC-kompatible Journalansetzungen zu erhalten (Datei 'Final_papers_crossref_enrichment.csv').
Aus dieser Datei kann dann zunächst einer Liste aller zu betrachtenden Journals für die Nicht-Teilnehmer extrahiert werden:



|Journal                                                               | Anzahl Artikel|
|:---------------------------------------------------------------------|--------------:|
|Atmospheric Chemistry and Physics                                     |             57|
|Biogeosciences                                                        |             45|
|Atmospheric Measurement Techniques                                    |             31|
|Geoscientific Model Development                                       |             19|
|Ocean Science                                                         |             15|
|Annales Geophysicae                                                   |             13|
|Climate of the Past                                                   |             12|
|Journal of Sensors and Sensor Systems                                 |             12|
|The Cryosphere                                                        |             12|
|Solid Earth                                                           |             11|
|Earth System Dynamics                                                 |             10|
|Proceedings of the International Association of Hydrological Sciences |             10|
|Earth System Science Data                                             |              9|
|Advances in Radio Science                                             |              8|
|Geographica Helvetica                                                 |              8|
|Hydrology and Earth System Sciences                                   |              8|
|Natural Hazards and Earth System Sciences                             |              7|
|Advances in Science and Research                                      |              5|
|Primate Biology                                                       |              5|
|SOIL                                                                  |              5|
|Advances in Geosciences                                               |              1|
|Archives Animal Breeding                                              |              1|
|Drinking Water Engineering and Science                                |              1|
|Earth Surface Dynamics                                                |              1|
|Fossil Record                                                         |              1|
|History of Geo- and Space Sciences                                    |              1|
|Nonlinear Processes in Geophysics                                     |              1|
|Web Ecology                                                           |              1|

Insgesamt handelt es sich hier um 28 Zeitschriften mit den bereits zuvor berechneten 310 Artikeln.

Diese Journals lassen sich nun in OpenAPC nachschlagen, wobei Durchschnittskosten berechnet werden. Grundsätzlich zeigt sich hierbei jedoch das Problem , dass die Datengrundlage an einigen Stellen zu dünn ist. Selbst wenn man die entsprechenden Artikel in OpenAPC nicht zusätzlich filtert (es wäre beispielsweise denkbar, sich bei der Durchschnittskostenberechnung ausschließlich auf deutsche Artikel aus dem Jahr 2016 zu beschränken, weil diese Parameter auch dem Copernicus-Datensatz zugrunde liegen), gibt es für einige Zeitschriften keine Kostendaten: 


|Journal                                                               | Anzahl| Anzahl in OpenAPC| OpenAPC-Durchnittskosten|
|:---------------------------------------------------------------------|------:|-----------------:|------------------------:|
|Atmospheric Chemistry and Physics                                     |     57|               362|                     1617|
|Biogeosciences                                                        |     45|               187|                     1436|
|Atmospheric Measurement Techniques                                    |     31|               158|                     1450|
|Geoscientific Model Development                                       |     19|                53|                     1416|
|Ocean Science                                                         |     15|                16|                     1244|
|Annales Geophysicae                                                   |     13|                62|                      732|
|Climate of the Past                                                   |     12|                66|                     1353|
|Journal of Sensors and Sensor Systems                                 |     12|                 0|                       NA|
|The Cryosphere                                                        |     12|                35|                     1263|
|Solid Earth                                                           |     11|                16|                     1338|
|Earth System Dynamics                                                 |     10|                 6|                      956|
|Proceedings of the International Association of Hydrological Sciences |     10|                 0|                       NA|
|Earth System Science Data                                             |      9|                 1|                     2056|
|Advances in Radio Science                                             |      8|                19|                      195|
|Geographica Helvetica                                                 |      8|                 0|                       NA|
|Hydrology and Earth System Sciences                                   |      8|                75|                     1324|
|Natural Hazards and Earth System Sciences                             |      7|                25|                     1013|
|Advances in Science and Research                                      |      5|                 4|                      213|
|Primate Biology                                                       |      5|                 1|                      643|
|SOIL                                                                  |      5|                 0|                       NA|
|Advances in Geosciences                                               |      1|                 3|                      442|
|Archives Animal Breeding                                              |      1|                 0|                       NA|
|Drinking Water Engineering and Science                                |      1|                 0|                       NA|
|Earth Surface Dynamics                                                |      1|                 3|                     1214|
|Fossil Record                                                         |      1|                 2|                      589|
|History of Geo- and Space Sciences                                    |      1|                 0|                       NA|
|Nonlinear Processes in Geophysics                                     |      1|                14|                      840|
|Web Ecology                                                           |      1|                 0|                       NA|

Die fehlenden Kostendaten stellen ein grundsätzliches Problem dar, insbesondere wenn es sich um Journals mit höherem Vorkommen handelt (Beispiel: Journal of Sensors and Sensor Systems). Es wären hier 3 Lösungen denkbar, die aber alle nicht optimal sind:

1. Komplettes Auslassen der Zeitschrift (verfälscht Ergebnisse beträchtlich)
2. Verwenden von (DOAJ-)Listenpreisen (Nicht identisch mit Kosten, nicht immer verfügbar)
3. Verwenden eines "Publisher-Durchschnitts": Es wird der Gesamtdurchschnitt aller Artikel/Zeitschriften des Verlags verwendet (potentiell sehr fehleranfällig, wenn es sich um ein Journal mit ungewöhnlich hohen oder niedrigen Preisen handelt)

Für diese Analyse wurden Listenpreise verwendet, teilweise aus dem DOAJ, teilweise von Hand recheriert (Datei 'journals_doaj_prices.csv'):


|Journal                                                               | Listenpreis|
|:---------------------------------------------------------------------|-----------:|
|Journal of Sensors and Sensor Systems                                 |         600|
|Proceedings of the International Association of Hydrological Sciences |          70|
|Geographica Helvetica                                                 |           0|
|SOIL                                                                  |        1000|
|Archives Animal Breeding                                              |          60|
|Drinking Water Engineering and Science                                |          69|
|History of Geo- and Space Sciences                                    |           0|
|Web Ecology                                                           |         600|

("Geographica Helvetica" erhebt keine APCs, für "History of Geo- and Space Sciences" war kein Preis zu ermitteln.)



Mit diesen Daten lassen sich jetzt ohne viel Aufwand die geschätzten Kosten für Copernicus-APCs einer Einrichtung im Jahr 2016 berechnen. Beispielhaft sei das hier vollständig ausgeführt für das GEOMAR:


|Journal                            | Anzahl| OpenAPC-Durchnittskosten| Gesamtkosten|
|:----------------------------------|------:|------------------------:|------------:|
|Biogeosciences                     |     18|                     1436|        25841|
|Atmospheric Chemistry and Physics  |      5|                     1617|         8084|
|Ocean Science                      |      5|                     1244|         6220|
|Earth System Science Data          |      2|                     2056|         4113|
|Atmospheric Measurement Techniques |      1|                     1450|         1450|
|Climate of the Past                |      1|                     1353|         1353|
|Earth System Dynamics              |      1|                      956|          956|
|Total                              |     33|                       NA|        48015|

Insgesamt hätte das GEOMAR also im Jahr 2016 schätzungsweise 48.000€ für 33 Copernicus-Publikationen gezahlt.

Der Vollständigkeit halber hier die kalkulierten Kosten für alle Nicht-Teilnehmer:


|Institution                  | Anzahl Artikel| geschätzte Gesamtkosten|
|:----------------------------|--------------:|-----------------------:|
|GEOMAR                       |             33|                   48015|
|TROPOS                       |             31|                   48820|
|DLR                          |             27|                   37215|
|AWI                          |             22|                   31677|
|HZG                          |             16|                   22965|
|PIK                          |             16|                   18175|
|Frankfurt U                  |             11|                   13274|
|Deutscher Wetterdienst       |             10|                    8418|
|TU Braunschweig              |             10|                    8137|
|Bonn U                       |              8|                    7993|
|Kiel U                       |              7|                    7951|
|Koeln U                      |              7|                    9538|
|Hamburg U                    |              6|                    6514|
|Thuenen                      |              6|                    8540|
|EUMETSAT                     |              5|                    6631|
|German Primate Center        |              5|                    3213|
|Hohenheim U                  |              4|                    5756|
|IAP                          |              4|                    3631|
|IASS                         |              4|                    6066|
|Jena U                       |              4|                    3986|
|RWTH                         |              4|                    5351|
|AUDI                         |              3|                     584|
|Augsburg U                   |              3|                    1450|
|BfG                          |              3|                     210|
|LIAG                         |              3|                    4014|
|PTB                          |              3|                    1633|
|Trier U                      |              3|                    2967|
|BGR                          |              2|                    2662|
|Eichstätt U                  |              2|                    1013|
|HZM                          |              2|                    3052|
|IKTS                         |              2|                    1200|
|IOW                          |              2|                    2680|
|Marburg U                    |              2|                    3066|
|MFN                          |              2|                    2025|
|Paderborn U                  |              2|                    1200|
|Senckenberg DZMB             |              2|                    2871|
|Stuttgart U                  |              2|                    2648|
|TU Darmstadt                 |              2|                    2955|
|TU Freiberg                  |              2|                    1283|
|TUHH                         |              2|                     390|
|Wuppertal U                  |              2|                    3233|
|AIP                          |              1|                     600|
|Beuth HS                     |              1|                    1450|
|Bochum HS                    |              1|                      70|
|Bremen Jacobs                |              1|                    1436|
|Deutsches Klimarechenzentrum |              1|                    1416|
|Duesseldorf U                |              1|                     600|
|Geisenheim U                 |              1|                    1436|
|Greifswald U                 |              1|                    1338|
|Halle-Wittenberg U           |              1|                    1000|
|HU Berlin                    |              1|                       0|
|IAMO                         |              1|                     600|
|IFW                          |              1|                     600|
|IGB                          |              1|                     956|
|IPM                          |              1|                     600|
|IRS                          |              1|                       0|
|IWS                          |              1|                     600|
|Koblenz U                    |              1|                      70|
|Magdeburg U                  |              1|                     600|
|OWL HS                       |              1|                     600|
|Reutlingen U                 |              1|                     195|
|Rostock U                    |              1|                    1000|
|Saarland U                   |              1|                     600|
|ZALF                         |              1|                    1000|
|ZMT                          |              1|                    1416|
|Gesamt                       |            310|                  371183|

